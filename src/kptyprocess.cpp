/*
    This file is part of the KDE libraries
    SPDX-FileCopyrightText: 2007 Oswald Buddenhagen <ossi@kde.org>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/

#include "kptyprocess.h"

#include <kpty_debug.h>
#include <kptydevice.h>
#include <kuser.h>

#include <stdlib.h>
#include <unistd.h>

//////////////////
// private data //
//////////////////

class KPtyProcessPrivate
{
public:
    KPtyProcessPrivate()
        : ptyChannels(KPtyProcess::NoChannels)
        , addUtmp(false)
    {
    }

    void _k_onStateChanged(QProcess::ProcessState newState)
    {
        if (newState == QProcess::NotRunning && addUtmp) {
            pty->logout();
        }
    }

    KPtyDevice *pty;
    KPtyProcess::PtyChannels ptyChannels;
    bool addUtmp : 1;
};

KPtyProcess::KPtyProcess(QObject *parent)
    : KProcess(parent)
    , d_ptr(new KPtyProcessPrivate)
{
    Q_D(KPtyProcess);

    d->pty = new KPtyDevice(this);
    if (!d->pty->open()) {
        qCWarning(KPTY_LOG) << "failed to open pty";
    }
    connect(this, SIGNAL(stateChanged(QProcess::ProcessState)), SLOT(_k_onStateChanged(QProcess::ProcessState)));
}

KPtyProcess::KPtyProcess(int ptyMasterFd, QObject *parent)
    : KProcess(parent)
    , d_ptr(new KPtyProcessPrivate)
{
    Q_D(KPtyProcess);

    d->pty = new KPtyDevice(this);
    if (!d->pty->open(ptyMasterFd)) {
        qCWarning(KPTY_LOG) << "failed to open pty fd" << ptyMasterFd;
    }
    connect(this, SIGNAL(stateChanged(QProcess::ProcessState)), SLOT(_k_onStateChanged(QProcess::ProcessState)));
}

KPtyProcess::~KPtyProcess()
{
    Q_D(KPtyProcess);

    if (state() != QProcess::NotRunning && d->addUtmp) {
        d->pty->logout();
        disconnect(SIGNAL(stateChanged(QProcess::ProcessState)), this, SLOT(_k_onStateChanged(QProcess::ProcessState)));
    }
    delete d->pty;
}

void KPtyProcess::setPtyChannels(PtyChannels channels)
{
    Q_D(KPtyProcess);

    d->ptyChannels = channels;
}

KPtyProcess::PtyChannels KPtyProcess::ptyChannels() const
{
    Q_D(const KPtyProcess);

    return d->ptyChannels;
}

void KPtyProcess::setUseUtmp(bool value)
{
    Q_D(KPtyProcess);

    d->addUtmp = value;
}

bool KPtyProcess::isUseUtmp() const
{
    Q_D(const KPtyProcess);

    return d->addUtmp;
}

KPtyDevice *KPtyProcess::pty() const
{
    Q_D(const KPtyProcess);

    return d->pty;
}

void KPtyProcess::setupChildProcess()
{
    Q_D(KPtyProcess);

    d->pty->setCTty();
    if (d->addUtmp) {
        d->pty->login(KUser(KUser::UseRealUserID).loginName().toLocal8Bit().constData(), qgetenv("DISPLAY").constData());
    }
    if (d->ptyChannels & StdinChannel) {
        Q_ASSERT(d->pty->slaveFd() >= 0);
        if (dup2(d->pty->slaveFd(), 0) < 0) {
            qCWarning(KPTY_LOG) << "Failed to dup() stdin: " << strerror(errno);
        }
    }
    if (d->ptyChannels & StdoutChannel) {
        Q_ASSERT(d->pty->slaveFd() >= 0);
        if (dup2(d->pty->slaveFd(), 1) < 0) {
            qCWarning(KPTY_LOG) << "Failed to dup2() stdout: " << strerror(errno);
        }
    }
    if (d->ptyChannels & StderrChannel) {
        Q_ASSERT(d->pty->slaveFd() >= 0);
        if (dup2(d->pty->slaveFd(), 2) < 0) {
            qCWarning(KPTY_LOG) << "Failed to dup2() stderr: " << strerror(errno);
        }
    }

    KProcess::setupChildProcess();
}

#include "moc_kptyprocess.cpp"
